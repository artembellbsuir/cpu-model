program CpuModel;

uses
  Vcl.Forms,
  MainForm in 'MainForm.pas' {Main},
  CPU in 'CPU.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMain, Main);
  Application.Run;
end.
