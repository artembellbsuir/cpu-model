unit CPU;

interface

const
   QueuesAmount = 3;

   ExecutorsAmount = 6;
   DataAmount = 11;

type
   TQueue = array of Integer;
   TQueues = array [0..2] of TQueue;
   TNextIndexes = array [0..2] of Integer;

   TDataCells = array [0..5] of array [0..10] of Integer;
   TOutputCells = array [0..5] of Integer;

   TCPU = class
      private
         FQueues: TQueues;
         FNextQueueIndex: Integer;
         FNextInQueue: TNextIndexes;

         FDataCells: TDataCells;
         FOutputCells: TOutputCells;

         FClockTime, FOutputTime: Integer;

         FTotalClocks, FTotalDowntime: Integer;


         function IsDone(): Boolean;
         procedure Clock();
         procedure ExecutorDoneClock(const Index: Integer);

         procedure DecrementOutput();
         procedure DecrementData(const Index: Integer);
         
         function IsAnyoneReady(): Boolean;
         function IsReadyForClock(const Index: Integer): Boolean;
         function IsExecutorDone(const Index: Integer): Boolean;

         function GetReadyExecutorIndex(): Integer;

      public
         property Clocks: Integer read FTotalClocks;
         property Downtime: Integer read FTotalDowntime;

         constructor Create(const ClockTime, OutputTime: Integer);
         procedure Start;

   end;

var
   MyCPU: TCPU;

implementation

{ TCPU }

const
   TemplateDataMatrix: TDataCells = (
      (4, 2, 3, 4, 6, 8, 9, 3, 2, 1, 0),
      (4, 2, 6, 8, 7, 3, 1, 2, 4, 6, 0),
      (2, 3, 4, 5, 9, 7, 6, 8, 3, 2, 2),
      (3, 3, 4, 2, 4, 6, 8, 9, 7, 2, 1),
      (2, 1, 2, 4, 5, 2, 3, 4, 6, 8, 9),
      (1, 2, 1, 6, 1, 9, 8, 1, 6, 8, 0)
   );


procedure TCPU.Clock;
var
   i, ExecutorIndex, TempIndex, StartIndex: Integer;
begin
   // get executor index according to priority
   FNextQueueIndex := 0;

   if IsAnyoneReady() then
   begin
      //first check highest priority queue

      FNextQueueIndex := GetReadyExecutorIndex();

      ///!!! if none of executors can do the job - we have downtime += clockTime!
      ///   dont forget to implement downtime inc function

      // perform some job
      // find first non-0 job unit and decrement it
      DecrementData(FNextQueueIndex);
      

      // calculate next value of FNextInQueue[FNextQueueIndex]
      ExecutorDoneClock(FNextQueueIndex);
   end
   else
   begin
      FTotalDowntime := FTotalDowntime + FClockTime;
   end;

   // decrement downtime of each executor
   DecrementOutput;
   // inc total clocks amount
   FTotalClocks := FTotalClocks + FClockTime;
end;

constructor TCPU.Create(const ClockTime, OutputTime: Integer);
var
  i, j: Integer;
begin
//   Inherited;

   FClockTime := ClockTime;
   FOutputTime := OutputTime;

   FTotalClocks := 0;
   FTotalDowntime := 0;

   // init queues
   SetLength(FQueues[0], 1);
   SetLength(FQueues[1], 3);
   SetLength(FQueues[2], 2);

   FQueues[0, 0] := 1;

   FQueues[1, 0] := 2;
   FQueues[1, 1] := 3;
   FQueues[1, 2] := 4;

   FQueues[2, 0] := 5;
   FQueues[2, 1] := 6;


   // init next indexes
   FNextQueueIndex := 0;

   FNextInQueue[0] := 0;
   FNextInQueue[0] := 0;
   FNextInQueue[0] := 0;

   //init data and downtime matrix
   for i := 0 to ExecutorsAmount - 1 do
   begin
      for j := 0 to DataAmount - 1 do
         FDataCells[i, j] := TemplateDataMatrix[i, j];

      FOutputCells[i] := 0;
   end;
end;



// it decrements only if executor is ready for work
procedure TCPU.DecrementData(const Index: Integer);
var
   i, CurrentData, NewOutput: Integer;
begin
   i := 0;
   while (FDataCells[Index, i] = 0) do
      Inc(i);

   CurrentData := FDataCells[Index, i];
   if CurrentData >= FClockTime then
      FDataCells[Index, i] := CurrentData - FClockTime
   else
   begin
      if FClockTime - CurrentData >= FOutputTime then
         FOutputCells[Index] := 0
      else
      begin
         FOutputCells[Index] := CurrentData + FOutputTime - FClockTime;
         FTotalDowntime := FTotalDowntime + (FClockTime - CurrentData);
      end;
      FDataCells[Index, i] := 0;
   end;
end;

procedure TCPU.DecrementOutput;
var
  i: Integer;
begin
   for i := 0 to ExecutorsAmount - 1 do
   begin
      if FOutputCells[i] > FOutputTime then
         FOutputCells[i] := FOutputCells[i] - FOutputTime
      else
         FOutputCells[i] := 0;
   end;
end;

procedure TCPU.ExecutorDoneClock(const Index: Integer);
begin
   case Index of
      1, 2, 3:
         FNextInQueue[1] := (FNextInQueue[1] + 1) mod 3;
      4, 5:
         FNextInQueue[2] := (FNextInQueue[2] + 1) mod 2;
   end;
end;

function TCPU.GetReadyExecutorIndex: Integer;
begin
   if IsReadyForClock(0) then
      Result := 0
   else if IsReadyForClock(1) or IsReadyForClock(2) or IsReadyForClock(3) then
   begin
      case FNextInQueue[1] of
         0: //2
            begin
               if IsReadyForClock(1) then
                  Result := 1
               else 
                  if IsReadyForClock(2) then
                     Result := 2
                  else
                     if IsReadyForClock(3) then
                        Result := 3;
            end;
         1: //3
            begin
               if IsReadyForClock(2) then
                  Result := 2
               else 
                  if IsReadyForClock(3) then
                     Result := 3
                  else
                     if IsReadyForClock(1) then
                        Result := 1;
            end;
         2: //4
         begin
            if IsReadyForClock(3) then
                  Result := 3
               else 
                  if IsReadyForClock(1) then
                     Result := 1
                  else
                     if IsReadyForClock(2) then
                        Result := 2;
         end;
      end;
   end
   else if IsReadyForClock(4) or IsReadyForClock(5) then
   begin
      case FNextInQueue[2] of
         0: //4
            begin
               if IsReadyForClock(4) then
                  Result := 4
               else 
                  if IsReadyForClock(5) then
                     Result := 5
            end;
         1: //5
            begin
               if IsReadyForClock(5) then
                  Result := 5
               else 
                  if IsReadyForClock(4) then
                     Result := 4
            end;
      end;
   end;
end;

function TCPU.IsAnyoneReady: Boolean;
var
  i: Integer;
begin
   Result := False;
   
   for i := 0 to ExecutorsAmount - 1 do
      if IsReadyForClock(i) then
         Result := True;   
end;

function TCPU.IsDone: Boolean;
var
  i, j: Integer;
begin
   Result := True;

   for i := 0 to ExecutorsAmount - 1 do
      if not IsExecutorDone(i) then
         Result := False;
end;

function TCPU.IsExecutorDone(const Index: Integer): Boolean;
var
  i: Integer;
begin
   Result := True;

   for i := 0 to DataAmount - 1 do
      if FDataCells[Index, i] <> 0 then
         Result := False;

   if FOutputCells[Index] <> 0 then
      Result := False;
end;

function TCPU.IsReadyForClock(const Index: Integer): Boolean;
begin
   Result := FOutputCells[Index] = 0;
end;

procedure TCPU.Start;
begin
   while not IsDone do
      Clock();
end;

end.
