unit MainForm;

interface

uses
   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
   System.Classes, Vcl.Graphics,
   Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CPU, Vcl.StdCtrls, Vcl.Grids;

type
   TMain = class(TForm)
      btnStart: TButton;
      Grid: TStringGrid;
      procedure FormCreate(Sender: TObject);
      procedure btnStartClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Main: TMain;

implementation

{$R *.dfm}

procedure TMain.btnStartClick(Sender: TObject);
begin
   MyCPU.Start;
end;

procedure TMain.FormCreate(Sender: TObject);
var
   i, j: Integer;
begin

   with Grid do
   begin
      ColWidths[0] := 100;
      Cells[0, 0] := '�����\�����';

      for i := 1 to 10 do
         Cells[i, 0] := IntToStr(i);

      for i := 0 to 10 do
         Cells[0, i + 1] := IntToStr(i);

      for i := 1 to 10 do
         for j := 0 to 10 do
         begin
            MyCPU := TCPU.Create(i, j);
            MyCPU.Start;

            Cells[i, j + 1] := FloatToStr((MyCPU.Clocks - MyCPU.Downtime) /
              MyCPU.Clocks)
         end;

   end;

   // MyCPU := TCPU.Create(9, 8);
   // MyCPU.Start;
   // ShowMessage(IntToStr(MyCPU.Clocks) + ' and ' + IntToStr(MyCPU.Downtime));
end;

end.
