object Main: TMain
  Left = 0
  Top = 0
  Caption = 'Main'
  ClientHeight = 363
  ClientWidth = 887
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnStart: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = btnStartClick
  end
  object Grid: TStringGrid
    Left = 8
    Top = 49
    Width = 871
    Height = 306
    ColCount = 11
    RowCount = 12
    TabOrder = 1
  end
end
